#!/usr/bin/env python3

from flask import Flask, render_template, redirect, url_for, send_file, request
import os
from urllib.parse import unquote


app = Flask(__name__)
app.secret_key = 'secret_key'

@app.route('/')
def home_page():
    return redirect(url_for('home'))
    
    
@app.route('/home', )
def home():
    filename = request.args.get('filename')
    if filename:
        return get_image(filename)
    else:
        return render_template('home.html')
    


def get_image(imagename):
    path = os.getcwd()
    full_path = path + '/static/files/' + imagename
    try:
        redirect(url_for('home'))
        return send_file(full_path, as_attachment=True)
    except FileNotFoundError:
        return render_template("not_found.html")


@app.route('/uploadfile', methods = ['POST'])
def uploadfile():
    query = request.args.get('location')
    if "../" in query:
        return render_template("badmn.html")
    
    
    if query:
        upload_destination = query
    else:
        upload_destination = '/static/files/'
        
    try:
        if request.method == 'POST':
          f = request.files['image_file']
          f.save(os.getcwd() + unquote(upload_destination) + f.filename)
          return render_template("gupload.html")
        else:
          return 'upload failed'
    except FileNotFoundError:
          return render_template("not_found.html")
    except IsADirectoryError:
          return redirect(url_for('home'))
   


if __name__ == "__main__":
    app.config['SESSION_TYPE'] = 'filesystem'
    sess.init_app(app)
    app.run(debug=True, host="0.0.0.0", port=8000)
