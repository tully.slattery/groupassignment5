
import requests

def print_error():
    print('\033[91m'+ 'unpatched' + '\033[0m')

def print_ok():
    print('\033[92m'+ 'patched' + '\033[0m')

def test_upload_vuln():
    print('  download vulnerability:  ', end='')

    s = requests.Session()

    files = {'image_file': ('home.html', open('upload_html','rb').read())}

    req = requests.Request(
        'POST', 
        'http://localhost:8000/uploadfile?location=/static/files/%252e%252e/%252e%252e/templates/',
        files=files
    )
    
    prepped = req.prepare()

    resp = s.send(prepped)

    if 'pwded' in str(requests.get('http://localhost:8000/home').content):
        print_error()
    else:
        print_ok()


def test_downloaded_vuln():
    print('  upload vulnerability:    ', end='')

    request = requests.get(
        'http://localhost:8000/home?filename=../../../../../../../../etc/passwd'
    )

    if 'root' in request.text:
        print_error()
    else:
        print_ok()

def main():
    print('---Testing vulnerabilities---')
    test_downloaded_vuln()
    test_upload_vuln()

if __name__ == '__main__':
    main()

