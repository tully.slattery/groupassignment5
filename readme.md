# Readme - CCSEP Group 5 Assignment 🐱‍💻

## Team Members

* Nicholas Klvana-Hooper: 19782944
* Lachlan Wells:          19467364
* Jarrod Ross:            19450186
* Tully Slattery:         id
* Nathan Harmer:          20160379

## Contribution

| Part of Assignment                | Name                   |
|-----------------------------------|------------------------|
| Presentation: Background          |                        |
| Presentation: Vulnerability       | Nathan Harmer          |
| Presentation: Impact              |                        |
| Presentation: Real-World Incident |                        |
| Presentation: Detection           | Nicholas Klvana-Hooper |
| Presentation: Prevention          |                        |
| Presentation: Conclusion          |                        |
| Presentation: Design              | Nicholas Klvana-Hooper |
| ------------------------------    | --------------------   |
| Vulnerable Program                | Nathan Harmer          |
| Patched Program                   |                        |
| Unit Test                         | Jarrod Ross            |
| Vulnerability Writeup             | Nicholas Klvana-Hooper |



## USING THE WEBAPP
make build - to firstly make the webapp and server
make run   - to make the server

## EXPLOITING THE WEBAPP

### Downloading a file

Upon opening the application you will be shown with this screen:

![](/images/1.png)

From here let's use Burp's proxy to capture the packets when we interact with the webapp to see what what is going on with the HTTP requests. Make sure that you go to the 'proxy' tab and enable intercept.

![](/images/2.png)

After doing this, go back to the webapp and press download, you should notice that we capture a HTTP GET request that looks like the following. Also notice at the top there is a query to a filepath. The default value is `coolestimage.jpg` which is the eye image we can see on the webpage.

![](/images/3.png)

Thinking about how we might be able to exploit this behaviour in the webapp, is perhaps injecting our own filepath in the query so it downloads a different file altogether. The general file that is targeted is the file `/etc/passwd`. If we think about it, currently this image filepath is perhaps in the root webapp folder, so we have to add some `../` before the `/etc/passwd` to navigate outside of this. After injecting our file path as follows, we will now download the `passwd` file from the webapp which we can now view.

![](/images/4.png)

![Exfiltrated 'etc/shadow' file](/images/5.png)


### Uploading a file

I have this picture of a dog, let's try the default submit and use Burp to intercept the request

![](/images/dog.jpg)

This looks like the following:

![](/images/6.png)

So let's try and do another path traversal injection into the file location in this GET request. Let's just try doing `/static/files/../../` in the location and forward that on, you'll see that our app tells us off for doing this.

![](/images/7.png)

So we can think that behind our app is somewhere checking if our file name includes the `../` probably. Sometimes there is a misconfiguration in the code for servers that doesn't URL decode the file path and checks. In the case of this example there is already a decode that occurs within flask so we're going to go for a double encoded payload.

When you send a request it will convert the '.' to '%2e'. But what if url encode it one more time, thus a double encode. Our payload was originally a `../` and then once encoded it is `%2e%2e/` and one more time it becomes `%252e%252e/`. 

Let's try put that in the file path now so we have something like `/static/files/%252e%252e/%252e%252e/%252e%252e/%252e%252e/%252e%252e/etc/`. What this should do is save our dog jpg into the `/etc` folder on the server. You can see the response on the right of the image below that shows it worked!

The repeater on Burp is what I used here as its easier to change your payload and test things. To get here, right click on your request from the proxy tab, now to 'sent to repeater' and it will add your GET request over there for you to change.

![](/images/8.png)

Now just to double check we'll sh into our docker container and look inside the `/etc/` folder, and look, our dog picture is there! Note that we could've checked that this existed by trying to download the file using the first vulnerability with a file path `../../../../../../../etc/dog.jpg` and you could see it this way.

![](/images/9.png)

### Final Thoughts

Because you can override some files, there is potential that you can launch a bit of a better attack than putting a dog image in a folder on the server. For intance I have made a new home.html file that looks like the following.

```
<html>
    <title>You have been pwnded ;)</title>
    <h1>It is my site now :3</h1>
</html>
```

Now I'll try the upload again with this file but I will interceot and change the file path to `/static/files/%252e%252e/%252e%252e/templates/`. You can see that this works in the following burp image:

![](/images/10.png)

Now if we try and reload our home page you can see that we have succesfully replaced the homepage of our webapp 😋

![](/images/11.png)






This webapp has some pretty clear vulnerabilities

If you can find the link that the button uses to query the file to download,
perhaps you can find a way to manipulate the link to download what you wish??



This webapp has some pretty clear vulnerabilities

If you can find the link that the button uses to query the file to download,
perhaps you can find a way to manipulate the link to download what you wish??

Basically:
http://localhost:8000/downloads?filename=<the world is your oyster>

e.g.
http://localhost:8000/downloads?filename=../../../app.py

you can also upload files which will be stored in an uploads directory

to access the docker file directory use 

docker exec -it <id_name> sh

where the id name is obtained using docker ps while the docker is running.
Its either/or the last column, or the hexadecimal in the first column
 
From here you can traverse the directory within the docker file structure

DOUBLE ENCODING PAYLOAD
%252e%252e/ = ../

